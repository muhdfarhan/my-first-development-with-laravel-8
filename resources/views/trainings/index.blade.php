@extends('admin.layouts.main')

@section('content')
<div class="container-fluid">
    <h1 class="mt-4">Training Module</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Training List</li>
    </ol>
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('alert'))
                <div class="alert {{session()->get('alert-type')}}" role="alert">
                    {{session()->get('alert')}}
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    {{ __('My Training List') }}
                    <div class="float-right">
                        <form action="" method="">
                            <div class="input-group">
                                <input type="text" class="form-control" name="keyword" value="{{(request()->get('keyword'))}}">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Creator</th>
                                {{-- <th>Created</th> --}}
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($trainings as $training)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$training->id}}</td>
                                    <td>{{$training->title}}</td>
                                    <td>{{$training->description}}</td>
                                    <td>{{$training->user->name}}</td>
                                    {{-- <td>{{$training->created_at}}</td> --}}
                                    <td>
                                        @can('view', $training)
                                            <a href="{{route('training:show', $training)}}" class="btn btn-primary">Show</a>
                                        @endcan
                                    </td>
                                    <td>
                                        @can('update', $training)
                                        <a href="{{route('training:edit', $training)}}" class="btn btn-success">Edit</a>
                                        @endcan
                                    </td>
                                    <td>
                                        @can('delete', $training)
                                        <a onclick="return confirm('Betul nak delete?')" href="{{route('training:delete', $training)}}" class="btn btn-danger">Delete</a>
                                        @endcan
                                    </td>
                                    
                                </tr>
                            @empty 
                                <td>No data</td>
                            @endforelse
                        </tbody>
                    </table>
                    {{
                        $trainings->appends([
                            'keyword' => request()->get('keyword')
                        ])->links()
                    }}
            </div>
        </div>
    </div>
</div>
@endsection