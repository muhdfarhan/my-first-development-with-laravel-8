@extends('admin.layouts.main')

@section('content')
<div class="container-fluid">
    <h1 class="mt-4">Your Training</h1>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Show Training') }}</div>
               
                <div class="card-body">
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" name="title" class="form-control" value="{{$training->title}}" readonly>
                    </div>

                    <div class="form-group">
                        <label>Description</label>
                        <textarea name="description" cols="20" rows="5" class="form-control" readonly>{{$training->description}} </textarea>
                    </div>

                    <div class="form-group">
                        {{-- <a href="{{asset('storage/'.$training->attachment)}}">View Picture</a> --}}
                        <a href="{{$training->attachment_url}}">View Picture</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection