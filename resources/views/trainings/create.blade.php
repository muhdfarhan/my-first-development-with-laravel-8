@extends('admin.layouts.main')

@section('content')
<div class="container-fluid">
    <h1 class="mt-4">Create Training</h1>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Training Create Form') }}</div>
                <div class="card-body">

                        <form action="" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" name="title" class="form-control">
                            </div>
    
                            <div class="form-group">
                                <label>Description</label>
                                <textarea name="description" cols="20" rows="5" class="form-control"></textarea>
                            </div>

                            <div class="form-group">
                                <label>Upload Picture</label>
                                <input type="file" name="attachment" class="form-control">
                            </div>
    
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Store My Training</button>
                            </div>
                        </form>
                        
                </div>
            </div>
        </div>
    </div>
</div>
@endsection