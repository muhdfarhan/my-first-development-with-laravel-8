<h1>Training Created!</h1>

<p>Training Details</p>
<p>
    Training Title: {{$training->title}}
    <br>
    Training Description: {{$training->description}}
    <br>
    Training Attachment: <img src='{{$training->attachment_url}}'>
</p>