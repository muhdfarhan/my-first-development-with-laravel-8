<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        // query all trainings from database using model
        $users = User::paginate(5);
        
        // return to view with all trainings
        // resources/views/trainings/index.blade.php
        return view('users.index', compact('users'));
    }
}
