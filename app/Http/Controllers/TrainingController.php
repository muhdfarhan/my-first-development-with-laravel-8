<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Training;
use \App\Mail\TrainingCreatedMail;
use \App\Jobs\SendEmailJob;
use Storage;
use File;
Use Mail;
class TrainingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //show data dalam laman utama
    public function index(Request $request)
    {
        if($request->keyword != null){
            //query based on keyword
            $trainings = Training::where('title', 'LIKE', '%'.$request->keyword.'%')->orwhere('description', 'LIKE', '%'.$request->keyword.'%')->paginate(5);
        } else {
            // query all trainings from database using model
            $trainings = Training::paginate(5); //default - per page 15 if not set value
        }
        
        // return to view with all trainings
        // resources/views/trainings/index.blade.php
        return view('trainings.index', compact('trainings'));
    }

    public function create()
    {
        // return view create form to user
        return view('trainings.create');
    }

    public function store(Request $request)
    {
        // changes to all branch
        // dd($request->all());
        //store data into db

        //Method 1 - POPO - Plain Old PHP Object
        // $training = new Training();
        // $training->title = $request->title;
        // $training->description = $request->description;
        // $training->user_id = auth()->user()->id;
        // $training->save();

        //Method 2 - Mass Assignments + Relationship
        $user = auth()->user();
        $training = $user->trainings()->create($request->only('title','description','attachment'));

        //check if has attachment
        if($request->hasFile('attachment')){
            //rename
            $filename = $training->id.'-'.date("Y-m-d").'.'.$request->attachment->getClientOriginalExtension();
            //save
            $training->update(['attachment' => $filename]); //mass assignment + protected fillable
            //store file
            Storage::disk('public')->put($filename, File::get($request->attachment));
        }

        //Method 1 - Cara Junior
        //send mail
        // Mail::send('emails.training-created', [
        //     'title' => $training->title,
        //     'description' => $training->description
        // ], function ($message) {
        //     $message->to('ad@gmail.com');
        //     //$message->(auth->$user());
        //     $message->subject('Training Created: Using Inline Mail Facade');
        // });

        //Method 2 - Send Email using Mailable Class
        // Mail::to('ad@gmail.com')->send(new TrainingCreatedMail($training));
        
        //Method 3 - Send Email with Jobs
        dispatch(new SendEmailJob($training));

        //return to /trainings
        return redirect('/trainings')
            ->with([
                'alert-type' => 'alert-primary',
                'alert' => 'Your training has been saved.'
            ]);
    }

    public function show(Training $training)
    {
        $this->authorize('view', $training);

        return view('trainings.show', compact('training'));
    }

    public function edit(Training $training)
    {
        $this->authorize('update', $training);

        return view('trainings.edit', compact('training'));
    }

    public function update(Training $training, Request $request)
    {
        // dd($request->all());
        //update data from form to db

        // Method 1 - POPO
        // $training->title = $request->title;
        // $training->description = $request->description;
        // $training->save();

        //Method 2 - Mass Assignments
        $training->update($request->only('title','description'));

        //return to /trainings
        return redirect()
            ->route('training:index')
            ->with([
                'alert-type' => 'alert-success',
                'alert' => 'Your training has been updated.'
            ]);
    }

    public function delete(Training $training)
    {
        $this->authorize('delete', $training);
        if ($training->attachment != null){
            Storage::disk('public')->delete($training->attachment);
        }
        //delete data from db
        $training->delete();
        //return to /trainings
        return redirect()->route('training:index')
        ->with([
            'alert-type' => 'alert-danger',
            'alert' => 'Your training has been deleted.'
        ]);
    }
}
